<?php

return [
	'remote_cache_seconds' => env('REMOTE_VIEW_CACHE_SECONDS'),
	'remote_view_url' => env('REMOTE_VIEW_URL')
];
