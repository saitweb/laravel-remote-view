<?php
/**
 * Created by PhpStorm.
 * User: eric.seyden
 * Date: 9/7/16
 * Time: 3:05 PM
 */

namespace Sait\RemoteView;

use Illuminate\Support\ServiceProvider;

class RemoteViewLocationServiceProvider extends ServiceProvider
{
	public function boot()
	{
		$remoteTemplateLocation = $this->app['config']['remote_view.remote_view_url'];
		$remoteTemplateLocation = str_replace('/','\\',$remoteTemplateLocation);

		if($remoteTemplateLocation === null and !\App::runningUnitTests())
		{
			throw new \Exception('REMOTE_VIEW_URL not configured');
		}
		$this->app['view']->share('remoteTemplateUrl',$remoteTemplateLocation);
	}
	
	public function register()
	{

	}
}