<?php namespace Sait\RemoteView;

use Illuminate\Filesystem\Filesystem;

class RemoteFilesystem {

    public function __construct(){

    }

    public function get($url)
    {
        return file_get_contents($url);
    }
}