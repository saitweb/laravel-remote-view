<?php namespace Sait\RemoteView;

use Illuminate\View\FileViewFinder;
use Carbon\Carbon;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Cache\CacheManager;

class RemoteViewFinder extends FileViewFinder {

    const DEFAULT_VIEW_CACHE_TIME = 604800;

    protected $storagePath;
    protected $cacheManager;
    protected $cacheTime;

    public function __construct($cacheTime,RemoteFilesystem $remoteFiles, Filesystem $files, CacheManager $cacheManager, $storagePath, array $paths, array $extensions = null)
    {
        parent::__construct($files,$paths,$extensions);
        $this->storagePath = $storagePath;
        $this->cacheManager = $cacheManager;
        $this->remoteFiles = $remoteFiles;
        $this->cacheTime = static::DEFAULT_VIEW_CACHE_TIME;
        if($cacheTime !== null)
        {
            $this->cacheTime = $cacheTime;
        }
    }

    public function find($name)
    {
        if($this->checkIfNameIsURLForLaravel5($name)) {
            $name = str_replace('\\', '/', $name);
        }
        if($this->checkIfNameIsURL($name)){
            $this->cacheRemoteView($name);
            return $this->views[$name] = $this->getRemoteViewLocalPath($name);
        }
        return parent::find($name);
    }

    private function checkIfNameIsURL($name)
    {
        return (strpos($name,'https://') !== false OR strpos($name,'http://') !== false);
    }

    private function checkIfNameIsURLForLaravel5($name)
    {
        return (strpos($name,'https:\\\\') !== false OR strpos($name,'http:\\\\') !== false);
    }

    private function getRemoteViewLocalPath($name)
    {
        $filename = $this->getRemoteViewFileName($name);
        return $this->storagePath.'/remote_view/'.$filename;
    }

    private function cacheRemoteView($name)
    {
        if($this->cacheTime === false){
            $viewContent = $this->remoteFiles->get($name);
            $this->storeRemoteViewLocally($name,$viewContent);
        }
        else if(!$this->cacheManager->has($name)){
            $expiresAt = Carbon::now()->addSeconds($this->cacheTime);
            $this->cacheManager->put($name,$this->remoteFiles->get($name),$expiresAt);
            $this->storeRemoteViewLocally($name,$this->cacheManager->get($name));
        }
    }

    private function storeRemoteViewLocally($name,$viewContent)
    {
        $localFilePath = $this->getRemoteViewLocalPath($name);

        $fileContent = null;
        if($this->files->isFile($localFilePath)) {
            $fileContent = $this->files->get($localFilePath);
        } else {
            $this->ensureRemoteViewStorageDirectoryExists();
        }
        $this->storeRemoteViewFileIfChanged($fileContent, $viewContent, $localFilePath);
    }

    protected function ensureRemoteViewStorageDirectoryExists()
    {
        $storageLocation = storage_path('remote_view');
        if(!$this->files->isDirectory($storageLocation)){
            $this->files->makeDirectory($storageLocation);
        }
    }

    private function getRemoteViewFileName($name)
    {
        return md5($name).'.blade.php';
    }

    /**
     * @param $fileContent
     * @param $cachedRemoteView
     * @param $localFilePath
     */
    private function storeRemoteViewFileIfChanged($fileContent, $cachedRemoteView, $localFilePath)
    {
        if (md5($fileContent) !== md5($cachedRemoteView)) {
            $this->files->put($localFilePath, $cachedRemoteView);
        }
    }
}