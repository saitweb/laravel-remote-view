<?php namespace Sait\RemoteView;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\ViewServiceProvider;

class RemoteViewServiceProvider extends ViewServiceProvider {

	public function registerViewFinder()
	{
		$this->app->bindShared('remote-files', function() { return new RemoteFilesystem; });

		$this->app->bindShared('view.finder', function($app)
		{
			$paths = $app['config']['view.paths'];
			$cacheTime = $app['config']['remote_view.remote_cache_seconds'];
			return new RemoteViewFinder($cacheTime,$app['remote-files'],$app['files'],$app['cache'],$app['path.storage'],$paths);
		});
	}

}
