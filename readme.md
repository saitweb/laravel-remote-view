Remote View Loader
==================

Remote View Loader allows users to specify http:// or https:// urls as view files.
These will be interpreted via the blade syntax and cached locally.
Compatable with Laravel 4 & 5

Laravel 5 issues 
================

 - Use backslashes when specifying the url directly in the @extends() blade call.'http:\\\\server.name\\directory' (The view factory runs the name through a normailization function that converts / to .) 
 - Alternatively use the Sait\RemoteView\RemoteViewLocationServiceProvider and use the remove_view.php configuration file to set the URL via a REMOTE_VIEW_URL environment variable, see optional config section.

Include the repo and package in your composer.json

```
#!json

	"repositories": [
	  {
		"type": "git",
		"url" : "git@bitbucket.org:saitweb/laravel-remote-view.git"
	  }
	],
	"require": {
		"sait/remote-view": "dev-master",
	},
        
```


After installing this package with composer you must alter app/config/app.php to load the service provider

```
#!php

'Sait\RemoteView\RemoteViewServiceProvider',

```

instead of 

```
#!php

'Illuminate\View\ViewServiceProvider'

```


Optional Configuration
======================

  - Copy remote_view.php from root of library to the conf directory in app.
  - Configure environment variables as needed, REMOTE_VIEW_CACHE_SECONDS, REMOTE_VIEW_URL
  - Set REMOTE_VIEW_CACHE_SECONDS to false to turn off caching for template work.
  - Include 'Sait\RemoteView\RemoteViewLocationServiceProvider' as a service provider in the app/config/app.php to share the location url variable $remoteTemplateUrl with all views 
  - This service provider shares the $remoteViewUrl variable for all views and handles changing the forward slashed to backslashes for laravel 5 comptability.